// JavaScript Document
function operacion(){
	
	//INICIALIZAMOS VARIABLES
	
	planemail = document.frmserv.planemail;
	planpuno = document.frmserv.planpua;
	planeuno = document.frmserv.planeua;
	planedos = document.frmserv.planeda;
	planetres = document.frmserv.planeta;
	planecuatro = document.frmserv.planeca;
	totaltotal = document.frmserv.totaltotal;
	
	//VALIDACIONES
	
	if(isNaN(planemail.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planemail.value = '';
		planemail.focus();
		return(false);
	}
	if(isNaN(planpuno.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planpuno.value = '';
		planpuno.focus();
		return(false);
	}
	if(isNaN(planeuno.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planeuno.value = '';
		planeuno.focus();
		return(false);
	}
	if(isNaN(planedos.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planedos.value = '';
		planedos.focus();
		return(false);
	}
	if(isNaN(planetres.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planetres.value = '';
		planetres.focus();
		return(false);
	}
	if(isNaN(planecuatro.value)){
		alert("Error:\nEste campo debe tener solo numeros.");
		planecuatro.value = '';
		planecuatro.focus();
		return(false);
	}
	
	//OPERACIONES
	resultadoemail = planemail.value * 4;
	resultadopuno = planpuno.value * 6;
	resultadoeuno = planeuno.value * 8;
	resultadoedos = planedos.value * 14;
	resultadoetres = planetres.value * 20;
	resultadoecuatro = planecuatro.value * 22;
	var i ;
		for (i=0;i<document.frmserv.serv.length;i++){ 
		if (document.frmserv.serv[i].checked) 
		break; 
		} 
		calc = document.frmserv.serv[i].value;
		
		total = ((resultadoemail * calc) + (resultadopuno * calc) + (resultadoeuno * calc) + (resultadoedos * calc) + (resultadoetres * calc) + (resultadoecuatro * calc));
		totals = parseFloat(total).toFixed(2);
		totaltotal.value = totals;

}