// JavaScript Document

function validar(){
	if(document.frmcontratar.nombre.value == 0){
		alert("Tiene que escribir su nombre")
		document.frmcontratar.nombre.focus()
		return 0;
	}
	if(document.frmcontratar.telefono.value == 0){
		alert("Tiene que escribir su telefono")
		document.frmcontratar.telefono.focus();
		return 0;
	}else
	if (isNaN(document.frmcontratar.telefono.value)) {
		alert("Error:\nEste campo debe tener solo numeros.");
		document.frmcontratar.telefono.value='';
		document.frmcontratar.telefono.focus();
		return (false);
		
 }
	
	if(document.frmcontratar.correo.value == 0){
		alert("Tiene que escribir su correo")
		document.frmcontratar.correo.focus();
		return 0;
	}else{
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address = document.frmcontratar.correo.value;
		if(reg.test(address) == false) {
			alert('Correo Invalido');
			document.frmcontratar.correo.focus();
			return false;
		}
	}
	
		
	document.frmcontratar.submit();

}