// JavaScript Document
function clickclear(thisfield, defaulttext) {
	if (thisfield.value == defaulttext) {
		thisfield.value = "";
	}
}


function validar(){
	if(document.frmlcontacto.nombre.value == 0){
		alert("Tiene que escribir su nombre")
		document.frmlcontacto.nombre.focus()
		return 0;
	}
	if(document.frmlcontacto.telefono.value == 0){
		alert("Tiene que escribir su telefono")
		document.frmlcontacto.telefono.focus();
		return 0;
	}else
	if (isNaN(document.frmlcontacto.telefono.value)) {
		alert("Error:\nEste campo debe tener solo numeros.");
		document.frmlcontacto.telefono.value='';
		document.frmlcontacto.telefono.focus();
		return (false);
		
 }
	
	if(document.frmlcontacto.correo.value == 0){
		alert("Tiene que escribir su correo")
		document.frmlcontacto.correo.focus();
		return 0;
	}else{
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address = document.frmlcontacto.correo.value;
		if(reg.test(address) == false) {
			alert('Correo Invalido');
			document.frmlcontacto.correo.focus();
			return false;
		}
	}
	
		
	document.frmlcontacto.submit();

}